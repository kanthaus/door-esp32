#ifndef INCLUDED_USERS_H
#define INCLUDED_USERS_H

#include <vector>
#include <string>

using namespace std;

class User {
   public:
      string name;
      string description;
      string serial;
      User(string name, string description, string serial) :
         name(name),
         description(description),
         serial(serial)
      { }
};

vector<User> users = {
  User(
    "some user",
    "some description of the RFID tag they use (or object it is attached to)",
    "01234567890abc" // the serial number of the tag
  ),
  User(
    "some other user",
    "some other description",
    "39872986498ad" // serial number
  ),
  // ...
};


#endif
