# Door ESP32

This repository contains a firmware for an ESP32 node designed to operate a door based on RFID tags.
The firmware is based on ESPHome and its configuration is stored in `door.yaml`.

## Warning
Because it is easy to copy the serial number of an RFID tag, tags can easily be copied, making this system unsafe for production use! 

## Instructions

Install ESPHome:
```
python3 -m venv .venv
source .venv/bin/activate
pip install esphome
```

Create a `secrets.yaml` file with the credentials required by the node (which can be found in our password store):
```
device_password: "some random password that protects Over The Air (OTA) firmware updates"
wifi_password: "the password of the kh-iot network"
hotspot_password: "some random password for the hotspot created by the device if it fails to connect to the wifi network"
```

Copy the `users_template.h` to `users.h` and add there the list of tags that should be allowed to open the door:
```
cp users_template.h users.h
```

For the initial set up, you need to connect the ESP32 to your computer to flash it. Once that initial set up is done, it will be able to use OTA updates. In both cases, updating the firmware is done with:
```
esphome run door.yaml
```

## Physical setup

This firmware assumes a ESP32 node connected to [a RC522 module](https://esphome.io/components/binary_sensor/rc522.html) via a SPI connection (see the configuration for the pins).

Another pin is connected to the door enable (which is itself connected to a step-down powering the door lock itself).
 
## Debugging

You can access a web interface for the node at http://192.168.5.40/.

## Misc

CC0 license

