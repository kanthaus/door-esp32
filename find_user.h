#ifndef INCLUDED_FIND_USER_CPP
#define INCLUDED_FIND_USER_CPP

#include <algorithm>
#include "users.h"

User* find_user(string serial) {
  size_t dashPos;
  while ((dashPos = serial.find("-")) != string::npos) {
      serial.replace(dashPos, 1, "");
  }
  transform(serial.begin(), serial.end(), serial.begin(), ::tolower);
  for (auto & user : users) {
     if (serial == user.serial) {
         return &user;
     }
  }
  return NULL;
}

#endif
